<?php

/**
 * @file Definition of MultiCriteriaContentRatingUserRatings.
 */

/**
 * This class is responsible to save or retrieve user ratings from daabase.
 */
class MultiCriteriaContentRatingUserRatings {

  /**
   * The name of table which user ratings are stored in.
   */
  private static $tableName = 'multi_criteria_content_rating_user_ratings';

  /**
   * Stores the given user ratings in database.
   *
   * This function uses merge query to insert/update
   * records.
   *
   * @param array $uploded_ratings
   *          User ratings information to update. Each index of array contains
   *          an associative array:
   *          vote_id: unique identifier of each rating record.
   *          criteria_id: identifier of criteria to be rated nid: identifier of
   *          content
   *          uid: identifier of user who rates
   *          timestamp: unix timestamp when user upload ratings
   *          value: An integer represent value of user rate for criteria.
   */
  public static function updateUserRatings(array $uploded_ratings) {
    foreach ($uploded_ratings as $ratings) {
      db_merge(self::$tableName)->key(array(
        'vote_id' => $ratings['vote_id'],
        'criteria_id' => $ratings['criteria_id'],
        'nid' => $ratings['nid'],
        'uid' => $ratings['uid']
      ))
        ->fields(array(
        'timestamp' => $ratings['timestamp'],
        'value' => $ratings['value']
      ))
        ->execute();
    }
  }

  /**
   * Returns user rates for the givetn content.
   *
   * Returns An array of user rates for the given content. Each index represents
   * one criteria.
   *
   * @param object $user
   *          Drupal user object.
   * @param object $node
   *          Drupal node object.
   */
  public static function getUserRatingsForContent($user, $node) {
    $query = db_select(self::$tableName, 'n');
    $query->fields('n');
    $query->condition('uid', $user->uid);
    $query->condition('nid', $node->nid);
    $result = $query->execute();
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $final_results = array();
    foreach ($result as $row) {
      $final_results[$row->criteria_id] = $row;
    }
    return $final_results;
  }

  /**
   * Returns user rating statistics for the given content.
   */
  public static function getUserRatingsResultForContent($node) {
    $query = db_select(self::$tableName, 'n');
    $query->condition('nid', $node->nid);
    $query->groupBy('criteria_id');
    $query->addField('n', 'criteria_id');
    $query->addExpression('COUNT(vote_id)', 'vote_id_count');
    $query->addExpression('AVG(value)', 'value_avg');
    $result = $query->execute();
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $final_results = array();
    foreach ($result as $row) {
      $final_results[$row->criteria_id] = $row;
    }
    return $final_results;
  }
}
