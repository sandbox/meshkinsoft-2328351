<?php

/**
 * @file
 * Implementation of Views hooks.
 */

/**
 * Implements hook_views_data().
 */
function multi_criteria_content_rating_views_data() {
  $data = array();
  $data['views']['MultiCriteriaContentRatingViewsHandlerRatingAvg'] = array(
    'field' => array(
      'title' => t('Average vote'),
      'help' => t('Provide average vote for content.'),
      'handler' => 'MultiCriteriaContentRatingViewsHandlerRatingAvg'
    )
  );
  return $data;
}
