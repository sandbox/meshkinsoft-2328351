<?php

/**
 * @file
 * Definition of views_handler_argument_mc_content_rating_vote_avg.
 */

/**
 * Define a new views handler to provide vale of field Average vote.
 */
class MultiCriteriaContentRatingViewsHandlerRatingAvg extends views_handler_field {

  /**
   * Overrides the parent query.
   */
  public function query() {
    // Do nothing.
  }

  /**
   * Overrides the parent option_definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Overrides the parent options_form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Overrides the parent render.
   */
  public function render($values) {
    module_load_include('class.inc', 'multi_criteria_content_rating', 'multi_criteria_content_rating_user_ratings');
    drupal_add_css(drupal_get_path('module', 'multi_criteria_content_rating') .
       '/multi_criteria_content_rating.css');
    $max_range = variable_get('mc_content_rating_max_range', 10);
    $content_vote_result = MultiCriteriaContentRatingUserRatings::getUserRatingsResultForContent($values);
    $total = 0;
    foreach ($content_vote_result as $vote) {
      $total += $vote->value_avg;
    }
    $avg = $total / count($content_vote_result);
    return multi_criteria_content_rating_rating_summary_results_output($avg, $max_range);
  }
}
