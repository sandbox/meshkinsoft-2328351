<?php

/**
 * @file
 * Implementation of administration pages.
 */

/**
 * Includes necessary class files.
 */
module_load_include('class.inc', 'multi_criteria_content_rating', 'multi_criteria_content_rating_criteria_modes');
module_load_include('class.inc', 'multi_criteria_content_rating', 'multi_criteria_content_rating_criteria');
module_load_include('class.inc', 'multi_criteria_content_rating', 'multi_criteria_content_rating_user_ratings');
module_load_include('class.inc', 'multi_criteria_content_rating', 'multi_criteria_content_rating_criteria_operations');

/**
 * Attaches criteria administration form elements to the givern form.
 */
function multi_criteria_content_rating_add_criteria_form_elements(&$form, &$form_state, $critesias, $entity_id, $entity_type) {
  $form['#tree'] = TRUE;
  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $entity_id
  );
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type
  );
  $form['criteria'] = array(
    '#type' => 'fieldset'
  );
  if (!empty($critesias)) {
    foreach ($critesias as $criteria) {
      $ind = $criteria->criteria_id;
      $form['criteria'][$ind] = array(
        '#type' => 'fieldset'
      );
      $form['criteria'][$ind]['id'] = array(
        '#type' => 'value',
        '#value' => $criteria->criteria_id
      );
      $form['criteria'][$ind]['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Criteria name'),
        '#default_value' => $criteria->criteria_name
      );
      $form['criteria'][$ind]['remove'] = array(
        '#type' => 'submit',
        '#name' => "remove-criteria-$ind",
        '#value' => t('Remove'),
        '#submit' => array(
          'multi_criteria_content_rating_global_settings_remove'
        )
      );
    }
  }
  $form['criteria']['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('New criteria')
  );
  $form['criteria']['new']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Criteria name')
  );
  $form['#validate'][] = 'multi_criteria_content_rating_add_criteria_form_elements_validate';
  $form['#submit'][] = 'multi_criteria_content_rating_add_criteria_form_elements_submit';
}

/**
 * Custom validation for additional criteria form elements.
 */
function multi_criteria_content_rating_add_criteria_form_elements_validate($form, &$form_state) {
  foreach ($form_state['values']['criteria'] as $key => $criteria) {
    if ($key != 'new' && empty($criteria['name'])) {
      form_set_error("criteria][$key][name", t('Criteria name cannot be empty.'));
    }
  }
}

/**
 * Processes submitedd data from additional criteria form elements.
 */
function multi_criteria_content_rating_add_criteria_form_elements_submit($form, &$form_state) {
  $entity_id = $form_state['values']['entity_id'];
  $entity_type = $form_state['values']['entity_type'];
  foreach ($form_state['values']['criteria'] as $key => $criteria) {
    if (!empty($criteria['name'])) {
      if (empty($criteria['id']) ||
         (!empty($criteria['id']) &&
         ($criteria['name'] != $form['criteria'][$key]['name']['#default_value']))) {
        $criteria_obj = new MultiCriteriaContentRatingCriteria();
        $criteria_obj->criteriaId = $criteria['id'];
        $criteria_obj->criteriaName = $criteria['name'];
        $criteria_obj->entityId = $entity_id;
        $criteria_obj->entityType = $entity_type;
        MultiCriteriaContentRatingCriteriaOperations::saveCriteria($criteria_obj);
      }
    }
  }
}

/**
 * Handles submited criteria remove button.
 */
function multi_criteria_content_rating_global_settings_remove($form, &$form_state) {
  $criteria_id = str_replace('remove-criteria-', '', $form_state['clicked_button']['#name']);
  if ($criteria_id) {
    MultiCriteriaContentRatingCriteriaOperations::removeCriteria($criteria_id);
  }
}

/**
 * Attaches additional form elemtns to the given form to manage view modes.
 */
function multi_criteria_content_rating_add_view_modes_form_elements(&$form, &$form_states) {
  $pseudo_formatters = multi_criteria_content_rating_get_formatters();
  $node_entity_info = entity_get_info('node');
  $node_entity_view_modes = $node_entity_info['view modes'];
  $node_entity_view_modes_options = array();
  foreach ($node_entity_view_modes as $key => $value) {
    $node_entity_view_modes_options[$key] = $value['label'];
  }
  $form['entity_view_modes'] = array(
    '#type' => 'fieldset'
  );
  $current_settings = variable_get('multi_criteria_content_rating_view_mode_settings', array());
  $ind = 0;
  foreach ($current_settings as $settings) {
    $form['entity_view_modes'][$ind] = array(
      '#type' => 'fieldset'
    );
    $form['entity_view_modes'][$ind]['type'] = array(
      '#type' => 'select',
      '#title' => t('Node type'),
      '#options' => variable_get('multi_criteria_content_rating_node_types', array()),
      '#default_value' => $settings['type']
    );
    $form['entity_view_modes'][$ind]['view_mode'] = array(
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => $node_entity_view_modes_options,
      '#default_value' => $settings['view_mode']
    );
    $form['entity_view_modes'][$ind]['formatter'] = array(
      '#type' => 'select',
      '#title' => t('Formatter'),
      '#options' => $pseudo_formatters,
      '#default_value' => $settings['formatter']
    );
    $ind++;
  }
  $form['entity_view_modes']['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('New formatter setting')
  );
  $form['entity_view_modes']['new']['type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#options' => array(
      '0' => t('None')
    ) + variable_get('multi_criteria_content_rating_node_types', array())
  );
  $form['entity_view_modes']['new']['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('View mode'),
    '#options' => array(
      '0' => t('None')
    ) + $node_entity_view_modes_options
  );
  $form['entity_view_modes']['new']['formatter'] = array(
    '#type' => 'select',
    '#title' => t('Formatter'),
    '#options' => array(
      '0' => t('None')
    ) + $pseudo_formatters
  );
  $form['#submit'][] = 'multi_criteria_content_rating_add_view_modes_form_elements_submit';
}

/**
 * Processes submitted data from addtitional view mode form elemtns.
 */
function multi_criteria_content_rating_add_view_modes_form_elements_submit($form, &$form_state) {
  $current_settings = variable_get('multi_criteria_content_rating_view_mode_settings', array());
  if (!empty($form_state['values']['entity_view_modes']['new']['type'])) {
    $form_state['values']['entity_view_modes'][] = $form_state['values']['entity_view_modes']['new'];
  }
  unset($form_state['values']['entity_view_modes']['new']);
  $arr = array_merge($current_settings, $form_state['values']['entity_view_modes']);
  $arr_helper = array();
  foreach ($arr as $key => $val) {
    if (!empty($arr_helper[$val['type']][$val['view_mode']][$val['formatter']])) {
      unset($arr[$key]);
    }
    else {
      $arr_helper[$val['type']][$val['view_mode']][$val['formatter']] = 1;
    }
  }
  variable_set('multi_criteria_content_rating_view_mode_settings', $arr);
}

/**
 * Builds module gobal settings form.
 */
function multi_criteria_content_rating_global_settings($form, &$form_state) {
  $critesias = MultiCriteriaContentRatingCriteriaOperations::getCriteria(NULL, NULL, 0, NULL);
  multi_criteria_content_rating_add_criteria_form_elements($form, $form_state, $critesias, 0, NULL);
  multi_criteria_content_rating_add_view_modes_form_elements($form, $form_state);
  $form['multi_criteria_content_rating_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowd Content types'),
    '#options' => multi_criteria_content_rating_get_node_types_options(),
    '#default_value' => variable_get('multi_criteria_content_rating_node_types', array())
  );
  $form['multi_criteria_content_rating_term_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxonomy term reference fields'),
    '#options' => multi_criteria_content_rating_get_term_fields_options(),
    '#required' => TRUE,
    '#default_value' => variable_get('multi_criteria_content_rating_term_fields', array())
  );
  $form['multi_criteria_content_rating_max_range'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum of rating range'),
    '#default_value' => variable_get('multi_criteria_content_rating_max_range', 10)
  );
  $form['multi_criteria_content_rating_coloring'] = array(
    '#type' => 'textarea',
    '#title' => t('Maximum of rating range'),
    '#default_value' => variable_get('multi_criteria_content_rating_coloring', multi_criteria_content_rating_coloring_default())
  );
  $form['#submit'][] = 'multi_criteria_content_rating_global_settings_submit';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  return $form;
}

/**
 * Processes submitted data from module global settings form.
 */
function multi_criteria_content_rating_global_settings_submit($form, &$form_state) {
  $allowd_types = array_diff($form_state['values']['multi_criteria_content_rating_node_types'], array(
    0
  ));
  $allowd_fields = array_diff($form_state['values']['multi_criteria_content_rating_term_fields'], array(
    0
  ));
  variable_set('multi_criteria_content_rating_node_types', $allowd_types);
  variable_set('multi_criteria_content_rating_max_range', $form_state['values']['multi_criteria_content_rating_max_range']);
  variable_set('multi_criteria_content_rating_term_fields', $allowd_fields);
  variable_set('multi_criteria_content_rating_coloring', $form_state['values']['multi_criteria_content_rating_coloring']);
}

/**
 * Prepares node types options for select.
 */
function multi_criteria_content_rating_get_node_types_options() {
  $options = array();
  $types = node_type_get_types();
  foreach ($types as $type => $value) {
    $options[$type] = $value->name;
  }
  return $options;
}

/**
 * Menu callback for taxonomy term specific criteria settings.
 */
function multi_criteria_content_rating_taxonomy_term_settings($taxonomy_term) {
  return drupal_get_form('multi_criteria_content_rating_entity_settings_form', $taxonomy_term->tid, 'taxonomy_term');
}

/**
 * Menu callback for node specific criteria settings.
 */
function multi_criteria_content_rating_node_settings($node) {
  return drupal_get_form('multi_criteria_content_rating_entity_settings_form', $node->nid, 'node');
}

/**
 * Form builder functon for node/taxonomy term specific criteria settings form.
 */
function multi_criteria_content_rating_entity_settings_form($form, &$form_state, $entity_id, $entity_type) {
  $critesias = MultiCriteriaContentRatingCriteriaOperations::getCriteria(NULL, NULL, $entity_id, $entity_type);
  $criteria_mode = MultiCriteriaContentRatingCriteriaModes::getCriteriaMode($entity_id, $entity_type);
  multi_criteria_content_rating_add_criteria_form_elements($form, $form_state, $critesias, $entity_id, $entity_type);
  $form['multi_criteria_content_rating_modes'] = array(
    '#type' => 'radios',
    '#title' => t('Criteria mode'),
    '#options' => multi_criteria_content_rating_get_criteria_modes_options(),
    '#weight' => -10,
    '#required' => TRUE,
    '#default_value' => empty($criteria_mode) ? MULTICRITERIA_RATING_USE_DEFAULT : $criteria_mode->mode
  );
  $form['criteria']['#states'] = array(
    'invisible' => array(
      ':input[name="multi_criteria_content_rating_modes"]' => array(
        'value' => MULTICRITERIA_RATING_USE_DEFAULT
      )
    )
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  $form['#submit'][] = 'multi_criteria_content_rating_entity_settings_form_submit';
  return $form;
}

/**
 * Submit handler functon for node/term specific criteria settings form.
 */
function multi_criteria_content_rating_entity_settings_form_submit($form, &$form_state) {
  $entity_id = $form_state['build_info']['args'][0];
  $entity_type = $form_state['build_info']['args'][1];
  $criteria_mode = new stdClass();
  $criteria_mode->entity_id = $entity_id;
  $criteria_mode->entity_type = $entity_type;
  $criteria_mode->mode = $form_state['values']['multi_criteria_content_rating_modes'];
  MultiCriteriaContentRatingCriteriaModes::saveCriteriaMode($criteria_mode);
}

/**
 * Prepares taxonomy term fields options for select.
 */
function multi_criteria_content_rating_get_term_fields_options() {
  $options = array();
  $fields = field_info_field_map();
  foreach ($fields as $field_name => $field_info) {
    if ($field_info['type'] == 'taxonomy_term_reference') {
      $options[$field_name] = $field_name;
    }
  }
  return $options;
}

/**
 * Prepares criteria selection mode options for select.
 */
function multi_criteria_content_rating_get_criteria_modes_options() {
  return array(
    MULTICRITERIA_RATING_USE_DEFAULT => t('Use global criteria'),
    MULTICRITERIA_RATING_ADDITIONAL => t('Additional criteria + global criteria'),
    MULTICRITERIA_RATING_OVERRIDE => t('Override global criteria')
  );
}
