<?php

/**
 * @file
 * Definition of MultiCriteriaContentRatingCriteriaModes.
 */

/**
 * Using top level mode.
 */
define('MULTICRITERIA_RATING_USE_DEFAULT', 1);

/**
 * Using top level criteria + current criteria.
 */
define('MULTICRITERIA_RATING_ADDITIONAL', 2);

/**
 * Using only defined criteria in current content or taxonomy term.
 */
define('MULTICRITERIA_RATING_OVERRIDE', 3);

/**
 * This class is responsible to save/retrieve ceriteria modes.
 *
 * This class is responsible to save/retrieve ceriteria modes of a content or a
 * taxonomy term.
 */
class MultiCriteriaContentRatingCriteriaModes {

  /**
   * The name of table which criteria modes are stored in.
   */
  private static $tableName = 'mc_content_rating_criteria_modes';

  /**
   * Returns criteria mode for the given entity type with the given entity id.
   *
   * Returns An array of criteria mode.
   *
   * @param int $entity_id
   *          The nid of content or tid of taxonomy term.
   * @param string $entity_type
   *          The entity type name: 'node' or 'taxonomy_term'.
   */
  public static function getCriteriaMode($entity_id, $entity_type) {
    $query = db_select(self::$tableName, 'n');
    $query->fields('n');
    if (!empty($entity_id)) {
      $query->condition('entity_id', $entity_id);
    }
    if (!empty($entity_type)) {
      $query->condition('entity_type', $entity_type);
    }
    $result = $query->execute();
    return $result->fetch(PDO::FETCH_OBJ);
  }

  /**
   * Insert/Update criteria mode for content/taxonomy term.
   *
   * @param object $criteria_mode
   *          A stdClass object with the follwing properties:
   *          entity_id: The nid of content or tid of taxonomy term.
   *          entity_type: The entity type name: 'node' or 'taxonomy_term'.
   *          mode: One of three constans.
   */
  public static function saveCriteriaMode($criteria_mode) {
    db_merge(self::$tableName)->key(array(
      'entity_id' => $criteria_mode->entity_id,
      'entity_type' => $criteria_mode->entity_type
    ))
      ->fields(array(
      'mode' => $criteria_mode->mode
    ))
      ->execute();
  }
}
