Introduction
----------------------------
There are a few modules to enable content rating in Drupal. Most of these modules only supports one criteria. So I decided to develop a module to enable site administrators to define multi-criteria rating system for modules. Also they could present average of ratings in content page and custom Views.

Sponsor
-----------------------------
This project is sponsored by Drupalika.
