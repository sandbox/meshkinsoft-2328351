<?php

/**
 * @file
 * Definition of MultiCriteriaContentRatingCriteriaOperations.
 */

/**
 * Loads class MultiCriteriaContentRatingCriteria.
 */
module_load_include('class.inc', 'multi_criteria_content_rating', 'multi_criteria_content_rating_criteria');

/**
 * Responsible to handle operations related to criteria.
 */
class MultiCriteriaContentRatingCriteriaOperations {

  /**
   * The name of table which criteria are stored in.
   */
  private static $tableName = 'multi_criteria_content_rating_criteria';

  /**
   * Inserts or updates the givern criteria object in database.
   *
   * @param MultiCriteriaContentRatingCriteria $content_rating_criteria
   *          of class MultiCriteriaContentRatingCriteria to be stored in
   *          database.
   */
  public static function saveCriteria(MultiCriteriaContentRatingCriteria $content_rating_criteria) {
    db_merge(self::$tableName)->key(array(
      'criteria_id' => $content_rating_criteria->criteriaId
    ))
      ->fields(array(
      'criteria_name' => $content_rating_criteria->criteriaName,
      'entity_id' => $content_rating_criteria->entityId,
      'entity_type' => $content_rating_criteria->entityType
    ))
      ->execute();
  }

  /**
   * Deletes criteria with the given criteria_id from database.
   *
   * @param int $criteria_id
   *          The unique identifier of criteria.
   */
  public static function removeCriteria($criteria_id) {
    db_delete(self::$tableName)->condition('criteria_id', $criteria_id)->execute();
    // @todo additional tasks after delete a criteria
  }

  /**
   * Returns criteria from database matching input params.
   *
   * Returns An array of criteria objects.
   *
   * @param int $criteria_id
   *          The unique identifier of criteria.
   * @param string $criteria_name
   *          The title of criteria.
   * @param int $entity_id
   *          The nid of content or tid of taxonomy term.
   * @param string $entity_type
   *          The entity type name: 'node' or 'taxonomy_term'.
   */
  public static function getCriteria($criteria_id, $criteria_name, $entity_id, $entity_type) {
    $query = db_select(self::$tableName, 'n');
    $query->fields('n');
    if (!empty($criteria_id)) {
      $query->condition('criteria_id', $criteria_id);
    }
    if (!empty($criteria_name)) {
      $query->condition('criteria_name', $criteria_name);
    }
    if (!empty($entity_id) || $entity_id === 0) {
      $query->condition('entity_id', $entity_id);
    }
    if (!empty($entity_type)) {
      $query->condition('entity_type', $entity_type);
    }
    else {
      $query->isNULL('entity_type');
    }
    $result = $query->execute();
    return $result->fetchAll(PDO::FETCH_OBJ);
  }

  /**
   * Returns criteria for the given node.
   */
  public static function getCriteriaForContent($node) {
    $field_term_reference_name = self::getTermField();
    $node_term_field = $node->$field_term_reference_name;
    $node_term_tid = $node_term_field[LANGUAGE_NONE][0]['tid'];
    $node_term = taxonomy_term_load($node_term_tid);
    $current_entity = array(
      'entity_id' => $node->nid,
      'entity_type' => 'node',
      'entity_object' => $node
    );
    $next_entity = array(
      'entity_id' => $node_term->tid,
      'entity_type' => 'taxonomy_term',
      'entity_object' => $node_term
    );
    $criteria_array = self::getCriteriaForEntity($current_entity, $next_entity);
    return $criteria_array;
  }

  /**
   * Returns criteria for the given entity.
   */
  public static function getCriteriaForEntity($current_entity, $next_entity) {
    $criteria_mode_obj = MultiCriteriaContentRatingCriteriaModes::getCriteriaMode($current_entity['entity_id'], $current_entity['entity_type']);
    $criteria_mode = empty($criteria_mode_obj) ? MULTICRITERIA_RATING_USE_DEFAULT : $criteria_mode_obj->mode;
    if ($criteria_mode == MULTICRITERIA_RATING_OVERRIDE) {
      return self::getCriteria(NULL, NULL, $current_entity['entity_id'], $current_entity['entity_type']);
    }
    elseif ($criteria_mode == MULTICRITERIA_RATING_ADDITIONAL ||
       $criteria_mode == MULTICRITERIA_RATING_USE_DEFAULT) {
      $next_criteria = array();
      if (!empty($next_entity)) {
        $next_next_term = taxonomy_get_parents($next_entity['entity_id']);
        $next_next_term = array_pop($next_next_term);
        if (!empty($next_next_term)) {
          $next_next_entity = array(
            'entity_id' => $next_next_term->tid,
            'entity_type' => 'taxonomy_term',
            'entity_object' => $next_next_term
          );
        }
        $next_criteria = self::getCriteriaForEntity($next_entity, $next_next_entity);
      }
      else {
        $next_criteria = self::getCriteria(NULL, NULL, 0, NULL);
      }
      if ($criteria_mode == MULTICRITERIA_RATING_ADDITIONAL) {
        $current_criteria = self::getCriteria(NULL, NULL, $current_entity['entity_id'], $current_entity['entity_type']);
        return array_merge($current_criteria, $next_criteria);
      } //
      elseif ($criteria_mode == MULTICRITERIA_RATING_USE_DEFAULT) {
        return $next_criteria;
      }
    }
    return array();
  }

  /**
   * Returns selected taxonomy term fields in global settings..
   */
  public static function getTermField() {
    $fields = variable_get('mc_content_rating_term_fields', array());
    if (!empty($fields)) {
      return array_pop($fields);
    }
    return '';
  }
}
