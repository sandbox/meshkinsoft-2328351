<?php

/**
 * @file
 * Definition of ContentRatingCriteria.
 */

/**
 * This class defines the structure of criteria object.
 */
class MultiCriteriaContentRatingCriteria {

  public $criteriaId;

  public $criteriaName;

  public $entityId;

  public $entityType;
}
